# Machine Learning
Documentation project for machine learning topics.
Recommender Systems.
Natural language processing.
Topic Modeling.

# Domain-Specific Channenges
## Context-based or context-aware recommender
time, location, or social data
예를 들어 옷의 추천에는 계절과 위치를 이용해야 함.

## Time-sensitive recommender
rating이 시간이 지남에 따라 변하는 경우
시간을 파라미터로 사용한다.

rating이 시간, 요일, 월, 계절에 따라 변하는 경우
(여름에 겨울옷의 추천은 의미가 없음)

## Location-based recommender
Foursquare
User-specific locality: preference locality
Item-specific locality: travel locality

## Social recommender

## Attac-resistent recommender
고의로 높거나 낮은 평점

## Group recommender
단체 영화 관람, 단체시설의 음악 선택
emotional contagion and confirmity


## Multi-criteria recommender
영화의 각본, 음악, 효과에 대해 별도의 평점

